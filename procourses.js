/**
 * @file
 * Adds Pro Courses widget javascript.
 */
(function ($) {

  Drupal.behaviors.procourses = {
    attach: function (context, settings) {
      if (context === document) {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'http://www.procourses.co.uk/javascripts/widgets.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      }
    }
  };

})(jQuery);
