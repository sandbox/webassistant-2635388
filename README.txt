
Pro Courses Module Readme
----------------------


Installation
------------

To install this module, place it in your sites/all/modules folder and enable it
on the modules page.


Configuration
-------------

All settings for this module are on the Pro Courses configuration page, under 
the Configuration section. You can visit the configuration page directly at 
admin/config/procourses.


To get your facilitator id, go to http://www.profaw.co.uk/en/classrooms/widgets 
and in the 'code for website' box you can see the id to the right of the text 
'data-facilitator-id='.
