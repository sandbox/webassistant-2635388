<?php

/**
 * @file
 * Adds a settings form so admin users can change widget settings.
 */

/**
 * Form: Pro Courses Widget settings.
 */
function procourses_admin_settings($form, &$form_state) {
  $form['procourses_facilitator_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facilitator ID'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('Your facilitator id can be found at !link. In the "code for website" box, look for the number next to "data-facilitator-id=".', array(
      '!link' => l(t('http://www.profaw.co.uk/en/classrooms/widgets'), 'http://www.profaw.co.uk/en/classrooms/widgets'),
    )),
    '#default_value' => variable_get('procourses_facilitator_id', ''),
  );
  $form['procourses_upcoming'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upcoming Courses Widget'),
  );
  $form['procourses_upcoming']['procourses_upcoming_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 7,
    '#default_value' => variable_get('procourses_upcoming_width', 465),
    '#field_suffix' => 'px',
  );
  $form['procourses_upcoming']['procourses_upcoming_days'] = array(
    '#type' => 'select',
    '#title' => t('Days'),
    '#options' => drupal_map_assoc(array(30, 60, 90, 120, 240, 365)),
    '#default_value' => variable_get('procourses_upcoming_days', 60),
  );
  $form['procourses_reviews'] = array(
    '#type' => 'fieldset',
    '#title' => t('Reviews Widget'),
  );
  $form['procourses_reviews']['procourses_reviews_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 7,
    '#default_value' => variable_get('procourses_reviews_width', 465),
    '#field_suffix' => 'px',
  );
  $form['procourses_video'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video of the Week Widget'),
  );
  $form['procourses_video']['procourses_video_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 7,
    '#default_value' => variable_get('procourses_video_width', 465),
    '#field_suffix' => 'px',
  );
  $form['procourses_video']['procourses_video_course_id'] = array(
    '#type' => 'select',
    '#title' => t('Days'),
    '#options' => array(
      '63' => t('50 Plus First Aid'),
      '107' => t('Activity First Aid Level 2'),
      '9' => t('Appointed Persons in the Workplace'),
      '94' => t('Asbestos Awareness'),
      '84' => t('Basic First Aid'),
      '132' => t('Basic Infection Control'),
      '78' => t('Basic Life Support for Dentists'),
      '123' => t('Beauty Therapy First Aid'),
      '233' => t('Beauty Therapy Infection Control'),
      '238' => t('Canine First Aid'),
      '216' => t('Care Certificate'),
      '77' => t('Cat Care'),
      '188' => t('Coaching'),
      '205' => t('Common Inductions Standards for Care'),
      '214' => t('Complaint Handling'),
      '86' => t('Control of Substances Hazardous to Health - COSHH Level 2'),
      '215' => t('CPR/BLS and AED for Healthcare Professionals'),
      '111' => t('CPR/BLS and AED Level 2'),
      '57' => t('CPR/BLS for Healthcare Professionals'),
      '189' => t('CPR/BLS for Healthcare Professionals Refresher'),
      '150' => t('Customer Service Level 2'),
      '44' => t('Data Protection'),
      '54' => t('Dementia Awareness'),
      '184' => t('Deprivation of Liberty Safeguards (DOLS)'),
      '76' => t('Dog Care'),
      '101' => t('DSE/VDU Operator Safety'),
      '58' => t('Education and Training'),
      '133' => t('Equality and Diversity Training'),
      '20' => t('Family First Aid'),
      '29' => t('Fire Safety Awareness Level 1'),
      '30' => t('Fire Safety Principles Level 2'),
      '5' => t('First Aid at Work Annual Refresher'),
      '74' => t('First Aid for Cyclists'),
      '23' => t('First Aid Plus'),
      '95' => t('Food Hygiene Level 1'),
      '31' => t('Food Safety Level 2'),
      '75' => t('Health and Safety Awareness Level 2'),
      '32' => t('Health and Safety Principles Level 1'),
      '140' => t('Healthcare Anaphylaxis'),
      '121' => t('Healthcare Health and Safety'),
      '202' => t('HeartSine AED'),
      '112' => t('Infection Control for Dentists'),
      '15' => t('Infection Control for Healthcare'),
      '134' => t('Information Governance'),
      '62' => t('Instructor Preparation'),
      '103' => t('Lone Workers'),
      '67' => t('Martial Arts First Aid'),
      '43' => t('Mental Capacity Act'),
      '24' => t('Moving People Safely Level 2'),
      '21' => t('Oxygen Provider'),
      '117' => t('Paediatric 1 day First Aid for Nannies and Au Pairs'),
      '217' => t('Paediatric First Aid Annual Refresher'),
      '22' => t('Pet First Aid'),
      '206' => t('Presentation Skills'),
      '18' => t('Pro Anaphylaxis Awareness'),
      '11' => t('Pro Paediatric'),
      '12' => t('ProAED'),
      '13' => t('ProBLS'),
      '42' => t('ProMedications'),
      '60' => t('ProTrainings Centre'),
      '61' => t('ProTrainings Skill Evaluator'),
      '46' => t('Risk Assessment Level 2'),
      '14' => t('Safe Moving and Handling Level 2'),
      '90' => t('Safeguarding of Children (Child Protection)'),
      '48' => t('Safeguarding of Vulnerable Adults'),
      '260' => t('Security Dog First Aid'),
      '19' => t('Sports First Aid'),
      '64' => t('Student First Aid'),
      '85' => t('Tattoo Infection Control'),
      '137' => t('Virtual Dementia Tour®'),
    ),
    '#default_value' => variable_get('procourses_video_course_id', 63),
  );
  return system_settings_form($form);
}

/**
 * Form Validation: Pro Courses Widget settings.
 */
function procourses_admin_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!is_numeric($values['procourses_upcoming_width'])) {
    form_set_error('procourses_upcoming_width', t('Only numeric values are allowed.'));
  }
  if (!is_numeric($values['procourses_reviews_width'])) {
    form_set_error('procourses_reviews_width', t('Only numeric values are allowed.'));
  }
  if (!is_numeric($values['procourses_video_width'])) {
    form_set_error('procourses_video_width', t('Only numeric values are allowed.'));
  }
}
